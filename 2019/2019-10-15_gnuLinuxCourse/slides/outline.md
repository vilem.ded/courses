#+TITLE: Entry Level GNU/Linux Course at LCSB


* Outline
** Before the shell
   - SSH
   - SSH Clients
   - Client installation Win (Mac?)
   - RSA key generation
   - Logging into a *nix server

** University's identity credentials management system : LUMS
   - Submitting keys
   - Other things one can do

** Effective shell usage
    Overview of the capabilities and possibilities.
*** Basics
**** Filesystem
     - ls/cd/mv/rm/cp
     - scp (rsync?)
     - what goes where (home,dev,proc,etc,usr)
**** Documentation systems
     - info
     - man
    
**** Permissions
     - (Super)users and groups
     - read, write, execute
     - chmod, chgrp, chown

**** Environment
     - environment variables
     - PATH, HOME
     - .profile and friends
     - source (.)
     
*** Effective use
**** SSH agent on remote server
     - adapt the (bash)_profile
**** History commands
     - !, !!, !-N, ^...^, ....
     
**** find

**** grep

**** sed

   


