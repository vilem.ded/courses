# Reproducibility crisis

<div align="center">
<img src="slides/img/reproCrisis.png">
</div>

*Baker, M., <a href="https://www.nature.com/news/1-500-scientists-lift-the-lid-on-reproducibility-1.19970">1,500 scientists lift the lid on reproducibility</a>, Nature 533, 452–454, 2016. doi:10.1038/533452a*