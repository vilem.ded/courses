# What is computer code?

**Computer code**: Set of instructions executed by a computer

**Programming language:**: Style of writing the set of instructions (e.g., C, C++, Fortran, Python, R, Matlab, Julia, …)

**Example:**

<div align="center">
<img src="slides/img/computerCode.png">
</div>

<div class="fragment">

<div align="center"><font color="#A52A2A"><span class="fas fa-arrow-right"></span> Maybe this is not so relevant … or even, why should I care?</font></div>
