# Quality of computer code is relevant for everyone.

`This does not concern me – I am only writing documentation or a script for generating a figure that I want to publish!`
[Anonymous researcher]

<div class="fragment">

**Great**, but ...

- Figure may not be reproducible
- Figure looks different when the input data changes
- Documentation will become outdated

… actually, **EVERYONE** writing documentation, a script, or code is concerned!

<div align="center">
<img src="slides/img/snoopy.png" height="400px">
</div>



# Attributes of high-quality computer code

**Quality** of computer code can be seen as a **group of various attributes**.

High-quality computer code should be:

1. <font color="#A52A2A">**Versioned**</font>: incremental code
2. <font color="#FFA500">**Well-written**</font> (formatted, documented, commented): easy to read by a human
3. <font color="#008931">**Tested**</font>: extensively tested code

<div align="center">
<img src="slides/img/qualitybadge.png">
</div>